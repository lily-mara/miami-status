package miami

type CourseSection struct {
	AcademicTerm             string
	CourseID                 string
	CourseCoe                string
	DeptCode                 string
	DeptName                 string
	SchoolName               string
	EnrollmentCountMax       int `json:",string"`
	EnrollmentCountCurrent   int `json:",string"`
	EnrollmentCountActive    int `json:",string"`
	EnrollmentCountAvailable int `json:",string"`
}

type CourseSectionList struct {
	CourseSections []*CourseSection `json:"courseSections"`
}

type CourseFree struct {
	CRN    string
	Emails []string
}

func (c *CourseSection) IsFree() bool {
	return c.EnrollmentCountAvailable > 0
}
