package miami

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/natemara/miami-status/config"
)

type Service struct{}

func (m *Service) GetSections(term string, CRNs []string) ([]*CourseSection, error) {
	response, err := http.Get(
		fmt.Sprintf(
			"https://ws.muohio.edu/courseSectionV2/%s.json?crn=%s",
			url.PathEscape(term),
			url.QueryEscape(strings.Join(CRNs, ",")),
		),
	)

	if err != nil {
		return []*CourseSection{}, err
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	sections := new(CourseSectionList)

	err = json.Unmarshal(bodyBytes, &sections)
	if err != nil {
		return []*CourseSection{}, err
	}

	return sections.CourseSections, nil
}

func (m *Service) GetFreeSections(term string, users []config.UserConfig) ([]*CourseFree, error) {
	crnToEmail := make(map[string][]string)

	for _, user := range users {
		for _, crn := range user.CRNs {
			emails, ok := crnToEmail[crn]
			if !ok {
				emails = make([]string, 0)
			}
			emails = append(emails, user.Email)
			crnToEmail[crn] = emails
		}
	}

	crnSlice := make([]string, 0)
	for crn := range crnToEmail {
		crnSlice = append(crnSlice, crn)
	}

	sections, err := m.GetSections(term, crnSlice)
	if err != nil {
		return []*CourseFree{}, err
	}

	results := make([]*CourseFree, 0)
	for _, section := range sections {
		free := section.IsFree()
		if !free {
			continue
		}

		emails, ok := crnToEmail[section.CourseID]
		if !ok {
			continue
		}

		results = append(
			results,
			&CourseFree{
				CRN:    section.CourseID,
				Emails: emails,
			},
		)
	}

	return results, nil
}
