package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/natemara/miami-status/config"
	"gitlab.com/natemara/miami-status/email"
	"gitlab.com/natemara/miami-status/miami"
)

func main() {
	config, err := config.Parse("./miami.toml")
	if err != nil {
		panic(err)
	}

	miamiService := miami.Service{}
	emailService := email.New(config.Mailgun)

	log.Println("We're on!")

	statusCheck := time.NewTicker(time.Minute * 10)
	go func() {
		go checkTick(&miamiService, &emailService, config)
		for _ = range statusCheck.C {
			go checkTick(&miamiService, &emailService, config)
		}
	}()

	pingTicker := time.NewTicker(time.Minute * 10)
	go func() {
		ping(config.PingURL)
		for _ = range pingTicker.C {
			ping(config.PingURL)
		}
	}()

	for {
		time.Sleep(time.Hour * 1)
	}
}

func checkTick(miamiService *miami.Service, emailService *email.EmailService, config *config.Config) {
	freeResults, err := miamiService.GetFreeSections(config.Term, config.User)
	if err != nil {
		log.Println(err)
		return
	}

	err = emailService.SendEmails(freeResults)
	if err != nil {
		log.Println(err)
		return
	}
}

func ping(url string) {
	res, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	err = res.Body.Close()
	if err != nil {
		panic(err)
	}
}
